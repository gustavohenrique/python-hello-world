FROM python
RUN adduser --system --disabled-password --home /home/python python
USER python
COPY . /home/python/app
EXPOSE 8080
CMD python -B /home/python/app/main.py

