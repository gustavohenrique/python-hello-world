from http.server import BaseHTTPRequestHandler, HTTPServer

class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        self.wfile.write('Hello World !'.encode())
        return

try:
    print ('Starting...')
    server = HTTPServer(('0.0.0.0', 8080), myHandler)
    print ('Started httpserver')
    server.serve_forever()
except KeyboardInterrupt:
    print ('^C received, shutting down the web server')
    server.server_close()

